!======================================================================
subroutine FEM2DextMass(XY1, XY2, XY3, &
&           lbT, lbB, lbV, dDATA, iDATA, iSYS, &
&           LDA, A, F, nRow, nCol, &
&           templateR, templateC)
!======================================================================
    implicit none
    include 'fem2Dtri.fd'
    include 'assemble.fd'
!======================================================================
    Real*8  XY1(*), XY2(*), XY3(*)
    Integer lbT, lbB(3), lbV(3)
   
    Real*8  dDATA(*)
    Integer iDATA(*), iSYS(*)
   
    Real*8  A(LDA, *), F(*)
    Integer templateR(*), templateC(*), LDA, nRow, nCol
   
    Integer  i, label, ir, ic
   
   ! Local variables
    Integer  ANI_D1x1_scalar
    External ANI_D1x1_scalar
   
!======================================================================
    nRow = 3
    nCol = 3
    
    ! ... set up templates
    Do i = 1, nRow
        templateR(i) = Vdof   
        templateC(i) = Vdof   
    End do
    
    ! ... compute the mass matrix 
    label = lbT
    
    Call fem2Dtri(XY1, XY2, XY3, &
    &              IDEN, FEM_P1, IDEN, FEM_P1, &
    &              label, ANI_D1x1_scalar, dDATA, iDATA, iSYS, 2, &
    &              LDA, A, ir, ic)
end







!===================================================================!
!                   Boundary condition function                     !
!                                                                   !
!                           Important:                              !
!  Using to get boundary type for all conditions and should return  !
!               right value on Dirichlet boundary                   !
!       To get value on another boundaries use another func         !
!                                                                   !
!    It is predefined here that diriclet boundary has label = 1     !
!                           be careful                              !
!===================================================================!
integer function Dbc(x, y, label, dDATA, iDATA, iSYS, Coef)
!===================================================================!
!                   Do not remove this declaration                  !
!===================================================================!
    implicit none
    include 'fem2Dtri.fd'

    double precision, intent(in)    :: dDATA(*), x, y
    integer, intent(in)             :: iDATA(*), label
    double precision, intent(out)   :: Coef(MaxTensorSize, *)
    integer, intent(out)            :: iSYS(*)
!===================================================================!
!                   Optional declaration here:                      !
!===================================================================!
!===================================================================!

    ! Output size, 1x1 -> a number
    iSYS(1) = 1
    iSYS(2) = 1

    if (label .eq. 3) then
        Dbc = BC_ROBIN
    else
        print*, "Boundary type is not Robin"
        Dbc = BC_NULL
        return
    end if

    ! For dirichlet boundary condition
    ! To return value for another boundary condition
    ! Use another func
    !Coef(1, 1) = 0.0
end function









!======================================================================
Subroutine FEM2DextSystem1(XY1, XY2, XY3, &
&           lbT, lbB, lbV, dDATA, iDATA, iSYS, &
&           LDA, A, F, nRow, nCol, &
&           templateR, templateC)
!======================================================================
    implicit none
    include 'fem2Dtri.fd'
    include 'assemble.fd'

!======================================================================
    Real*8  XY1(*), XY2(*), XY3(*)

    Integer lbT, lbB(3), lbV(3)
    Real*8  dDATA(*)
    Integer iDATA(*), iSYS(*)

    Integer templateR(*), templateC(*), LDA, nRow, nCol
    Real*8  A(LDA, *), F(*)

    !Local variables
    Integer  Ddiff1, Dbc, Dbcv1, DbcRobCoef1
    External Ddiff1, Dbc, Dbcv1, DbcRobCoef1

    Integer  i, j, k, label, ir, ic, ibc
    Real*8   B(3, 3), x, y, eBC(1)
    Logical  ifXbc

    double precision      :: XYN(2)
    double precision      :: XYP(2, 3)

    Integer  iref(4)
    DATA     iref/1, 2, 3, 1/
    integer    l, m

    Integer  ANI_D1x1_scalar
    External ANI_D1x1_scalar
! ======================================================================
    nRow = 3
    nCol = 3

! ... set up templates (we have 1 group, skipping additional bits)

    Do i = 1, nRow
        templateR(i) = Vdof ! + Group1
    End do

    Do k = 1, nCol
        templateC(k) = templateR(k)
    End do

    ! ... compute the stiffness matrix 
    label = lbT

    Call fem2Dtri(XY1, XY2, XY3, &
    &              GRAD, FEM_P1, GRAD, FEM_P1, &
    &              label, Ddiff1, dDATA, iDATA, iSYS, 2, &
    &              LDA, A, ir, ic)

    Call fem2Dtri(XY1, XY2, XY3, &
    &              IDEN, FEM_P1, IDEN, FEM_P1, &
    &              label, ANI_D1x1_scalar, dDATA, iDATA, iSYS, 2, &
    &              3, B, ir, ic)

    do i = 1, 3
    do j = 1, 3
        A(i, j) = A(i, j) + B(i, j)
    end do
    end do

    ! ... impose boundary conditions (assume nRow = nCol) at nodes of triangle
    
    Do i = 1, 2
    XYP(i, 1) = XY1(i)
    XYP(i, 2) = XY2(i)
    XYP(i, 3) = XY3(i)
    End do

    Do k = 1, 3
        If(lbB(k).ne.0) Then
            l = iref(k + 1)
            m = iref(l + 1)

            x = (XYP(1, k) + XYP(1, l)) / 2.0
            y = (XYP(2, k) + XYP(2, l)) / 2.0
        
            ibc = Dbc(x, y, lbB(k), dDATA, iDATA, iSYS, eBC)
            call calNormalExt(XYP(1, k), XYP(1, l), XYP(1, m), XYN(1))
            dData(3) = XYN(1)
            dData(4) = XYN(2)
                
            If(ifXbc(ibc, BC_ROBIN)) Then
                label = lbB(k)

                Call fem2Dedge(XYP(1, k), XYP(1, l), XYP(1, m), &
                & IDEN, FEM_P1, IDEN, FEM_P1, &
                & label, DbcRobCoef1, dDATA, iDATA, iSYS, 2, &
                & 3, B, ir, ic)

                A(k, k) = A(k, k) + B(1, 1)
                A(l, l) = A(l, l) + B(2, 2)
                A(k, l) = A(k, l) + B(1, 2)
                A(l, k) = A(l, k) + B(2, 1)
            End if
        End if
    End do
end subroutine

!======================================================================
Subroutine FEM2DextRh1(XY1, XY2, XY3, &
&           lbT, lbB, lbV, dDATA, iDATA, iSYS, &
&           LDA, A, F, nRow, nCol, &
&           templateR, templateC)
!======================================================================
    implicit none
    include 'fem2Dtri.fd'
    include 'assemble.fd'

!======================================================================
    Real*8  XY1(*), XY2(*), XY3(*)

    Integer lbT, lbB(3), lbV(3)
    Real*8  dDATA(*)
    Integer iDATA(*), iSYS(*)

    Integer templateR(*), templateC(*), LDA, nRow, nCol
    Real*8  A(LDA, *), F(*)

    !Local variables
    Integer  Drhs1, Dbc, Dbcv1
    External Drhs1, Dbc, Dbcv1

    Integer  i, j, k, label, ir, ic, ibc
    Real*8   x, y, eBC(1), G(3)
    Logical  ifXbc

    double precision      :: XYN(2)
    double precision      :: XYP(2, 3)

    Integer  iref(4)
    DATA     iref/1, 2, 3, 1/
    integer    l, m
! ======================================================================
    nRow = 3
    nCol = 3

! ... set up templates (we have 1 group, skipping additional bits)

    Do i = 1, nRow
        templateR(i) = Vdof ! + Group1
    End do

    Do k = 1, nCol
        templateC(k) = templateR(k)
    End do

    ! ... compute the stiffness matrix 
    label = lbT

    ! ... compute right hand side
    Call fem2Dtri(XY1, XY2, XY3, &
    &              IDEN, FEM_P0, IDEN, FEM_P1, &
    &              lbT, Drhs1, dDATA, iDATA, iSYS, 2, &
    &              1, F, ir, ic)

    ! ... impose boundary conditions (assume nRow = nCol) at nodes of triangle
    
    Do i = 1, 2
    XYP(i, 1) = XY1(i)
    XYP(i, 2) = XY2(i)
    XYP(i, 3) = XY3(i)
    End do

    Do k = 1, 3
        If(lbB(k).ne.0) Then
            l = iref(k + 1)
            m = iref(l + 1)

            x = (XYP(1, k) + XYP(1, l)) / 2.0
            y = (XYP(2, k) + XYP(2, l)) / 2.0
        
            ibc = Dbc(x, y, lbB(k), dDATA, iDATA, iSYS, eBC)
            call calNormalExt(XYP(1, k), XYP(1, l), XYP(1, m), XYN(1))
            dData(3) = XYN(1)
            dData(4) = XYN(2)

            If(ifXbc(ibc, BC_NEUMANN) .OR. &
                &  ifXbc(ibc, BC_ROBIN)) Then

                label = lbB(k)

                Call fem2Dedge(XYP(1, k), XYP(1, l), XYP(1, m), &
                    & IDEN, FEM_P0, IDEN, FEM_P1,  &
                    & label, Dbcv1, dDATA, iDATA, iSYS, 2,  &
                    & 1, G, ir, ic)
                F(k) = F(k) + G(1)
                F(l) = F(l) + G(2)
            End if
        End if
    End do
end subroutine


!===================================================================!
!                       Diffusion tensor                            !
!===================================================================!
Integer Function Ddiff1(x, y, label, dDATA, iDATA, iSYS, Coef)
!===================================================================!
!                   Do not remove this declaration                  !
!===================================================================!
    implicit none
    include 'fem2Dtri.fd'

    double precision, intent(in)    :: dDATA(*), x, y
    integer, intent(in)             :: iDATA(*), label
    double precision, intent(out)   :: Coef(MaxTensorSize, *)
    integer, intent(out)            :: iSYS(*)
!===================================================================!
!                   Optional declaration here:                      !
!===================================================================!
!===================================================================!


    iSYS(1) = 1
    iSYS(2) = 1

    Coef(1, 1) = 1d-3
    Ddiff1 = TENSOR_SCALAR
End

integer function DbcRobCoef1(x, y, label, dDATA, iDATA, iSYS, Coef)
!===================================================================!
!                   Do not remove this declaration                  !
!===================================================================!
    implicit none
    include 'fem2Dtri.fd'

    double precision, intent(in)    :: dDATA(*), x, y
    integer, intent(in)             :: iDATA(*), label
    double precision, intent(out)   :: Coef(MaxTensorSize, *)
    integer, intent(out)            :: iSYS(*)
!===================================================================!
!                   Optional declaration here:                      !
!===================================================================!
!===================================================================!

    iSYS(1) = 1
    iSYS(2) = 1

    if (label .eq. 3) then
        DbcRobCoef1 = BC_ROBIN_COEF
    else
        print*, "Non Robin boundary condition!"
        DbcRobCoef1 = BC_NULL
        return
    end if

    Coef(1, 1) = 1d0
end function

!===================================================================!
!                     Boundary value function                       !
!===================================================================!
integer function Dbcv1(x, y, label, dDATA, iDATA, iSYS, Coef)
!===================================================================!
!                   Do not remove this declaration                  !
!===================================================================!
    implicit none
    include 'fem2Dtri.fd'

    double precision, intent(in)    :: dDATA(*), x, y
    integer, intent(in)             :: iDATA(*), label
    double precision, intent(out)   :: Coef(MaxTensorSize, *)
    integer, intent(out)            :: iSYS(*)
!===================================================================!
!                   Optional declaration here:                      !
!===================================================================!
    external                        :: DbcRobCoef1, RealFunc
    integer                         :: DbcRobCoef1

    double precision                :: RobCoef, RealFunc
    integer                         :: tmp
    external                        :: XDerRealFunc, YDerRealFunc
    double precision                :: XDerRealFunc, YDerRealFunc

    external                        :: Ddiff1
    integer                         :: Ddiff1
    double precision                :: diff
    double precision, parameter             :: M_PI = 4d0 * ATAN(1d0)

!===================================================================!

    if (label .eq. 3) then
        Dbcv1 = BC_ROBIN
    else
        print*, "Boundary type is not Robin"
        Dbcv1 = BC_NULL
        return
    end if

    tmp = DbcRobCoef1(x, y, label, dData, iData, iSys, Coef)
    RobCoef = Coef(1, 1)

    tmp = Ddiff1(x, y, label, dData, iData, iSys, Coef);
    diff = Coef(1, 1)

    ! Output size, 1x1 -> a number
    iSYS(1) = 1
    iSYS(2) = 1

    Coef(1, 1) = RobCoef * RealFunc(x, y, dData(2)) + &
    & (dData(3) * XDerRealFunc(x, y, dData(2)) + &
    & dData(4) * YDerRealFunc(x, y, dData(2))) * diff
    ! Coef(1, 1) = 0d0
    ! if (x .eq. 0d0 .or. x .eq. 1d0) then
    !     Coef(1, 1) = Coef(1, 1) + sin(6d0 * M_PI * y) * exp(-0.052 * M_PI * M_PI * dData(2))
    ! end if
    ! if (y .eq. 0d0) then
    !     Coef(1, 1) = Coef(1, 1) - cos(4d0 * M_PI) * exp(-0.052 * M_PI * M_PI * dData(2)) * &
    !     & 0.001 * 6d0 * M_PI
    ! end if
    ! if (y .eq. 1d0) then
    !     Coef(1, 1) = Coef(1, 1) + cos(4d0 * M_PI) * exp(-0.052 * M_PI * M_PI * dData(2)) * &
    !     & 0.001 * 6d0 * M_PI
    ! end if

    ! if (    x .eq. 0d0 .and. y .eq. 0d0 .or. &
    ! &       x .eq. 1d0 .and. y .eq. 0d0 .or. &
    ! &       x .eq. 0d0 .and. y .eq. 1d0 .or. &
    ! &       x .eq. 1d0 .and. y .eq. 1d0 ) then
    !     Coef(1, 1) = Coef(1, 1) / 2d0
    ! end if
end function

integer function Drhs1(x, y, label, dDATA, iDATA, iSYS, Coef)
!===================================================================!
!                   Do not remove this declaration                  !
!===================================================================!
    implicit none
    include 'fem2Dtri.fd'

    double precision, intent(in)    :: dDATA(*), x, y
    integer, intent(in)             :: iDATA(*), label
    double precision, intent(out)   :: Coef(MaxTensorSize, *)
    integer, intent(out)            :: iSYS(*)
!===================================================================!
!                   Optional declaration here:                      !
!===================================================================!
    external                        :: LapRealFunc
    external                        :: TDerRealFunc
    double precision                :: LapRealFunc
    double precision                :: TDerRealFunc
    external                        :: Ddiff1
    integer                         :: Ddiff1
    integer                         :: tmp
    double precision                :: diff
!===================================================================!

    tmp = Ddiff1(x, y, label, dData, iData, iSys, Coef);
    diff = Coef(1, 1)

    ! Output size, 1x1 -> a number
    iSYS(1) = 1
    iSYS(2) = 1

    Drhs1 = TENSOR_SCALAR

    Coef(1, 1) = -LapRealFunc(x, y, dData(2)) * diff + TDerRealFunc(x, y, dData(2))
    !Coef(1, 1) = 0d0
end function

!===================================================================!
!                       Known solution                              !
!===================================================================!
integer function Fu1(x, y, label, dDATAFU, iDATAFU, iSYS, coef)
!===================================================================!
!                   Do not remove this declaration                  !
!===================================================================!
    implicit none
    include 'fem2Dtri.fd'

    double precision, intent(in)    :: dDATAFU(*), x, y
    integer, intent(in)             :: iDATAFU(*), label
    double precision, intent(out)   :: Coef(MaxTensorSize, *)
    integer, intent(out)            :: iSYS(*)
!===================================================================!
!                   Optional declaration here:                      !
!===================================================================!
    external                        :: RealFunc
    double precision                :: RealFunc
!===================================================================!

    ! Output size, 1x1 -> a number
    iSYS(1) = 1
    iSYS(2) = 1
    Fu1 = TENSOR_SCALAR

    Coef(1, 1) = RealFunc(x, y, dDataFU(2))
end function









!===================================================================!
!                   Some testing functions                          !
!===================================================================!
double precision function RealFunc(x, y, t)
implicit none
double precision :: x, y, t
double precision, parameter             :: M_PI = 4d0 * ATAN(1d0)
    !RealFunc = dsin(5d0*x) * dsin(5d0*y) * exp(t) + 1d0
    RealFunc = (1d0 + x + y) * sin(x) * t * t + 1d0
    !RealFunc = (x * x + y * y) + t + 1d0
    !RealFunc = (x * x * x + y * y * y) * t * t + 1d0
    !RealFunc = (1d0 + x + y) * sin(x)
    !RealFunc = cos(M_PI * 4d0 * x) * sin(6d0 * M_PI * y) * &
    !            exp(-M_PI * M_PI * 0.001 * 52d0 * t)
end function

double precision function LapRealFunc(x, y, t)
implicit none
double precision :: x, y, t
double precision, parameter             :: M_PI = 4d0 * ATAN(1d0)
    !LapRealFunc = -(25d0 + 25d0) * dsin(5d0*x) * dsin(5d0*y) * exp(t)
    LapRealFunc = 2d0 * t *t * cos(x) - (1d0 + x + y) * t *t* sin(x)
    !LapRealFunc = 4d0
    !LapRealFunc = 6d0 * (x + y) * t * t
    !LapRealFunc = 2d0 * cos(x) - (1d0 + x + y) * sin(x)
    !LapRealFunc = -M_PI * M_PI * (4d0 * 4d0 + 6d0 * 6d0) * &
    !            cos(M_PI * 4d0 * x) * sin(6d0 * M_PI * y) * &
    !            exp(-M_PI * M_PI * 0.001 * 52d0 * t)
end function

double precision function TDerRealFunc(x, y, t)
    implicit none
    double precision :: x, y, t
    double precision, parameter             :: M_PI = 4d0 * ATAN(1d0)
    !TDerRealFunc = dsin(5d0*x) * dsin(5d0*y) * exp(t)
    TDerRealFunc = (1d0 + x + y) * sin(x) * t * 2d0;
    !TDerRealFunc = 1d0
    !TDerRealFunc = (x * x * x + y * y * y) * t * 2d0
    !TDerRealFunc = 0d0
    !TDerRealFunc = -M_PI * M_PI * 0.001 * 52d0 * &
    !                cos(M_PI * 4d0 * x) * sin(6d0 * M_PI * y) * &
    !                exp(-M_PI * M_PI * 0.001 * 52d0 * t)
end function
  
double precision function XDerRealFunc(x, y, t)
    implicit none
    double precision :: x, y, t
    double precision, parameter             :: M_PI = 4d0 * ATAN(1d0)
    !XDerRealFunc = 5d0 * dcos(5d0*x) * dsin(5d0*y) * exp(t)
    XDerRealFunc = t * t * (  sin(x) + (1 + x + y) * cos(x)  )
    !XDerRealFunc = 2d0 * x
    !XDerRealFunc = 3d0 * x * x * t * t
    !XDerRealFunc = (  sin(x) + (1 + x + y) * cos(x)  )
    !XDerRealFunc = -4d0 * M_PI * sin(M_PI * 4d0 * x) * sin(6d0 * M_PI * y) * &
        !exp(-M_PI * M_PI * 0.001 * 52d0 * t)
 end function
  
double precision function YDerRealFunc(x, y, t)
    implicit none
    double precision :: x, y, t
    double precision, parameter             :: M_PI = 4d0 * ATAN(1d0)
    !YDerRealFunc = 5d0 * dsin(5d0*x) * dcos(5d0*y) * exp(t)
    YDerRealFunc = sin(x) * t * t
    !YDerRealFunc = 2d0 * y
    !YDerRealFunc = 3d0 * y * y * t * t
    !YDerRealFunc = sin(x)
    !YDerRealFunc = 6d0 * M_PI * cos(M_PI * 4d0 * x) * cos(6d0 * M_PI * y) * &
        !exp(-M_PI * M_PI * 0.001 * 52d0 * t)
end function

