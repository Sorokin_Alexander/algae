FORTRAN = gfortran

LIB_DIR = lib

LDFLAGS += -L$(LIB_DIR)
LDLIBS += -lrcb2D-3.1 -lfem2D-3.1 -lview2D-3.1 -lmba2D-3.1 -lumfpack -lblas


.PHONY: all clean

all: algae

algae: main.o mesh.o umfpack.o forlibfem.o
	$(FORTRAN) $^ $(LDFLAGS) $(LDLIBS) -o $@
	
main.o: main.f95 mesh.o umfpack.o forlibfem.o
	$(FORTRAN) -c $< -o $@

forlibfem.o: forlibfem.f95
	$(FORTRAN) -c $< -o $@

mesh.o: mesh.f95
	$(FORTRAN) -c $< -o $@

umfpack.o: umfpack.f95
	$(FORTRAN) -c $< -o $@
clean:
	$(RM) *.o *.mod
