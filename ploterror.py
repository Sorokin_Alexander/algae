#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("results.txt")
sc = plt.scatter(data[:, 0], data[:, 1], c=data[:, 2])
plt.colorbar(sc)
plt.show()