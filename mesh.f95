module mesh
    ! Old version with some small changes
    implicit none

    private                     :: markDirB
    private                     :: RefineRule

    !nvmax - maximum number of mesh nodes
    integer, parameter          :: NVMAX = 140000

    !ntmax - maximum number of mesh triangles
    integer, parameter          :: NTMAX = 2 * NVMAX

    !nbmax - maximum number of boundary edges
    integer, parameter          :: NBMAX = NVMAX / 112

    integer                     :: nv, nt, nb
    integer                     :: tri(3, NTMAX), labelT(NTMAX)
    integer                     :: bnd(2, NBMAX), labelB(NBMAX)
    double precision            :: vrt(2, NVMAX)
    integer                     :: labelV(NVMAX)

    contains

    integer function markDirB(x, y, label, dDATA, iDATA, iSYS, Coef)
    !===================================================================!
    !                   Do not remove this declaration                  !
    !===================================================================!
        implicit none
        include 'fem2Dtri.fd'

        double precision, intent(in)    :: dDATA(*), x, y
        integer, intent(in)             :: iDATA(*), label
        double precision, intent(out)   :: Coef(MaxTensorSize, *)
        integer, intent(out)            :: iSYS(*)
    !===================================================================!
    !                   Optional declaration here:                      !
    !===================================================================!
    !===================================================================!
        if (label .eq. 1) then
            markDirB = BC_DIRICHLET
        else
            markDirB = BC_NULL
        end if

        return
    end function

    logical function GenerateSimpleMesh(kind, splitLevel)
            implicit none
        !============================================================!
        !                         Variables                          !
        !============================================================!
            integer, intent(in)                 :: kind
            integer, intent(in)                 :: splitLevel

            integer, parameter                  :: maxlevel = 20

            !work memory (at least 11*ntmax+7)
            integer, parameter                  :: MaxWi = 2 * (11 * NTMAX + 7)
            integer                             :: iW(MaxWi)

            !history of bisection
            Logical                             ::history(maxlevel*NTMAX)

            !local variables
            Integer                             ::ilevel, iERR


            integer                             :: iData(1), iSys(1)
            double precision                    :: dData(1)
        !============================================================!
        !                       Generation                           !
        !============================================================!

        if (kind .eq. 2) then

            nv = 3
            vrt(1, 1) = 0d0
            vrt(2, 1) = 1d0
            vrt(1, 2) = 0d0
            vrt(2, 2) = 0d0
            vrt(1, 3) = 1d0
            vrt(2, 3) = 0d0

            nt = 1
            tri(1, 1) = 1
            tri(2, 1) = 2
            tri(3, 1) = 3

            nb = 3
            bnd(1, 1) = 1!
            bnd(2, 1) = 2!
            bnd(1, 2) = 2!
            bnd(2, 2) = 3!
            bnd(1, 3) = 3!
            bnd(2, 3) = 1!

            ! Labels for identifying boundaries (for example for boundary conditions)
            labelB(1) = 1
            labelB(2) = 1
            labelB(3) = 1

        else if (kind == 1) then

            nv = 4
            vrt(1, 1) = 0d0
            vrt(2, 1) = 1d0
            vrt(1, 2) = 0d0
            vrt(2, 2) = 0d0
            vrt(1, 3) = 1d0
            vrt(2, 3) = 0d0
            vrt(1, 4) = 1d0
            vrt(2, 4) = 1d0

            nt = 2
            tri(1, 1) = 1
            tri(2, 1) = 2
            tri(3, 1) = 3
            tri(1, 2) = 3
            tri(2, 2) = 4
            tri(3, 2) = 1

            nb = 4
            bnd(1, 1) = 1!                    label 3
            bnd(2, 1) = 2!
            bnd(1, 2) = 2!                   ^------->
            bnd(2, 2) = 3!     label 2       |       |      label 4
            bnd(1, 3) = 3!                   |       |
            bnd(2, 3) = 4!                   |<------V
            bnd(1, 4) = 4!                     label 1
            bnd(2, 4) = 1!

            ! Labels for identifying boundaries (for example for boundary conditions)
            labelB(1) = 3
            labelB(2) = 3
            labelB(3) = 3
            labelB(4) = 3

        else
            print*, "Wrong figure kind"
            GenerateSimpleMesh = .false.
            return
        end if

            call graph_demo(nv,vrt, nt,tri, 'mesh/mesh_initial.ps','')

            iERR = 0
            call InitializeRCB(nt, NTMAX, vrt, tri, MaxWi, iW, iERR)

            If(iERR.GT.0) then
                Call errMesRCB(iERR, 'main', &
                                           'error in InitializeRCB (mesh problem)')
                GenerateSimpleMesh = .false.
                return
            end if

            if (splitLevel > maxlevel) then
                print*, "Split level is too big"
                GenerateSimpleMesh = .false.
            end if

            do ilevel = 1, splitLevel
                call LocalRefine(nv, NVMAX, nb, NBMAX, nt, NTMAX, &
                &        vrt, tri, bnd, labelB, labelT,           &
                &        RefineRule, ilevel, maxlevel, history,   &
                &        MaxWi, iW, iERR)
            end do

            call graph_demo(nv,vrt, nt,tri, 'mesh/mesh_final.ps', '')




            call markDIR(nv, vrt, labelV, nb, bnd, labelB, markDirB, dDATA, iDATA, iSYS)

            GenerateSimpleMesh = .true.
            return
        end function

        subroutine RefineRule(nt, tri, vrt, verf, ilevel)
            implicit none

            integer, intent(in)          ::  nt, tri(3,*), ilevel
            double precision, intent(in) ::  vrt(2,*)
            integer, intent(out)         ::  verf(*)

            integer  i

            do i = 1, nt
                verf(i) = 1
            end do

        end subroutine


end module
