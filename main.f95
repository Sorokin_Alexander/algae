program algae
    use mesh
    use mUmfpack
    implicit none
    Include 'fem2Dtri.fd'
    Include 'assemble.fd'

    ! Command line arguments variables
    integer, parameter              :: CMD_ARG_NUM = 2
    integer                         :: cmd_arg_count
    character(len=10)               :: cmd_arg_value

    ! System discretization parameters
    integer                         :: bisection_number
    double precision                :: tau
    double precision, parameter     :: finish_time = 1.0

    ! Arrays for linear systems
    integer                         :: nRow, nCol
    ! I* - for first nonzero in row (?column)
    ! J* - for index in column (?row)
    ! M* - for elements themselves
    ! R* - for righthand vector
    ! S* - for solution vector
    integer, allocatable            :: IA(:), JA(:)
    double precision, allocatable   :: MA(:), RA(:), SA(:)

    integer, allocatable            :: IB(:), JB(:)
    double precision, allocatable   :: MB(:), RB(:), SB(:)

    integer, allocatable            :: IM(:), JM(:)
    double precision, allocatable   :: MM(:)

    integer, allocatable            :: IT(:), JT(:)
    double precision, allocatable   :: MT(:), RT(:)


    ! Umfpack
    type(c_ptr)                     :: symbolic,numeric
    real(8)                         :: control(0:UMFPACK_CONTROL-1),info(0:UMFPACK_INFO-1)
    integer, parameter              :: sys = UMFPACK_A
    integer                         :: status = UMFPACK_STATUS
    integer, parameter              :: statusOK = UMFPACK_OK
    target                          :: IA, JA, MA, CONTROL, info

    ! AniFem  
    integer, parameter              :: namax = 900000
  
    integer, parameter              :: MaxWr = 1000000
    integer, parameter              :: MaxWi = 5000000
  
    integer                         :: iW(MaxWi)
    double precision                :: rW(MaxWr)

    double precision                :: dDATAFEM(4)
    ! dData(1) - inversed tau
    ! dData(2) - current time
    ! dData(3) - reserved for
    ! dData(4) - boundary normal
    integer                         :: iDATAFEM(2)

    integer                         :: controlFEM(3)
    
    external FEM2DextMass, FEM2DextSystem1, FEM2DextRh1

    ! Local variables
    integer                         :: i
    integer                         :: ntime
    double precision                :: cur_time


    ! Testing
    double precision                :: error
    external RealFunc
    double precision                RealFunc


    double precision                J
    external sol_error
    double PRECISION sol_error
!=========================================================================================
  
    cmd_arg_count = command_argument_count()
    if (cmd_arg_count .ne. CMD_ARG_NUM) then
        write(*, "(a,I2,a,I2,a)") "Invalid command line arguments number. Expected: ", CMD_ARG_NUM, ". Got: ", cmd_arg_count, "."
        call exit(1)
    end if
  
    ! Change this if u changing command line processing
    call get_command_argument(1, cmd_arg_value)
    read(cmd_arg_value, *) bisection_number
    write(*, "(a,I2)") "Bisection number: ", bisection_number
  
    call get_command_argument(2, cmd_arg_value)
    read(cmd_arg_value, *) tau
    write(*, "(a,f6.3)") "Tau: ", tau
  
  
    ! Mesh generating
    if (GenerateSimpleMesh(1, bisection_number) .neqv. .true.) then
        write(*, "(a)") "Error at mesh generating routine"
        call exit(1)
    end if
    

    allocate(IM(NVMAX), JM(NAMAX), MM(NAMAX))
    allocate(IA(NVMAX), JA(NAMAX), MA(NAMAX), RA(NVMAX), SA(NVMAX))
    allocate(IB(NVMAX), JB(NAMAX), MB(NAMAX), RB(NVMAX), SB(NVMAX))
    allocate(IT(NVMAX), JT(NAMAX), MT(NAMAX), RT(NVMAX))
  
    controlFEM(1) = IOR(MATRIX_GENERAL, FORMAT_CSC)
    controlFEM(2) = 0
  
    dDATAFEM(1) = 1d0 / tau
  
    Call BilinearFormTemplate( &
    &        nv, nb, nt, vrt, labelV, bnd, labelB, tri, labelT, &
    &        FEM2DextMass, dDATAFEM, iDATAFEM, controlFEM, &
    &        nvmax, namax, IM, JM, MM, RT, nRow, nCol, &
    &        MaxWi, MaxWr, iW, rW)
    Call CSC2CSC0(nCol, IM, JM)

    Call BilinearFormTemplate( &
    &        nv, nb, nt, vrt, labelV, bnd, labelB, tri, labelT, &
    &        FEM2DextSystem1, dDATAFEM, iDATAFEM, controlFEM, &
    &        nvmax, namax, IA, JA, MA, RT, nRow, nCol, &
    &        MaxWi, MaxWr, iW, rW)
    Call CSC2CSC0(nCol, IA, JA)
    Call umf4def(control)
    control(1) = 1 !????
    Call umf4sym(nCol, nCol, Ia,Ja,MA, symbolic, control, info)
    Call umf4num(IA,JA,MA, symbolic, numeric, control, info)
    Call umf4fsym(symbolic)
  
    ntime = finish_time / tau
  
    cur_time = 0d0
    do i = 1, nv
        SA(i) = RealFunc(vrt(1, i), vrt(2, i), cur_time)
    end do
  
    J = 0d0
    do i = 1, ntime
        cur_time = cur_time + tau
        dDATAFEM(1) = 1d0 / tau
        dDATAFEM(2) = cur_time

        Call BilinearFormTemplate( &
        &        nv, nb, nt, vrt, labelV, bnd, labelB, tri, labelT, &
        &        FEM2DextRh1, dDATAFEM, iDATAFEM, controlFEM, &
        &        nvmax, namax, IT, JT, MT, RA, nRow, nCol, &
        &        MaxWi, MaxWr, iW, rW)
    
        Call mulAcsc0(nRow, IM, JM, MM, SA, RT)
        Call daxpy(nRow, 1d0, RT,1, RA,1)
        Call umf4sol(sys, SA, RA, numeric, control, info)
        if (info(status) .ne. statusOK) then
            write(*, *) "Solving trouble!!!!!!!!!!!!!"
            call c_umfpack_di_report_info(c_null_ptr, c_loc(info))
        end if
    
        
        error = sol_error(cur_time, vrt, nt, tri, SA, 2d0)
        J = J + tau * error * error
        write(*, *) i, error
    end do
    print*, J, sqrt(J)
  
    open (unit=12,file="results.txt",action="write",status="replace")
    do i = 1, nv
        write(12, *) vrt(1, i), vrt(2, i), (abs(RealFunc(vrt(1, i), vrt(2, i), finish_time) - SA(i)) / &
        & abs(RealFunc(vrt(1, i), vrt(2, i), finish_time))), SA(i), RealFunc(vrt(1, i), vrt(2, i), 1d0)
    end do
    close (12)

    call umf4fnum(numeric)
    deallocate(IM, JM, MM)
    deallocate(IA, JA, MA, RA, SA)
    deallocate(IB, JB, MB, RB, SB)
    deallocate(IT, JT, MT, RT)
end program algae
  
  
       double precision function sol_error(t, vrt, nt, tri, value, Lp)
        implicit none
        Include 'fem2Dtri.fd'
          Include 'assemble.fd'
        integer, intent(in)             :: nt, tri(3,*)
        double precision, intent(in)    :: vrt(2,*), value(*)
        double precision, intent(in)    :: Lp, t
  
        integer                         :: i
        double precision                :: trierr, Uloc(3)
        integer                         :: label
  
        external                        ANI_D1x1_one, FU1
        integer                         ANI_D1x1_one, FU1
  
        Real*8   ddata(2)
        Integer  idata(1)
        integer isys(MAXiSYS)
  
  
        label = 1           ! Labeles unused
  
        sol_error = 0d0
        do i = 1, nt
          
          Uloc(1) = value(tri(1, i))
          Uloc(2) = value(tri(2, i))
          Uloc(3) = value(tri(3, i))
  
  
  
  
          dData(1) = t
          dData(2) = t
  
          !write(*, *) vrt(1, tri(1, i)), vrt(2, tri(1, i)), Uloc(1)
          !write(*, *) vrt(1, tri(2, i)), vrt(2, tri(2, i)), Uloc(2)
          !write(*, *) vrt(1, tri(3, i)), vrt(2, tri(3, i)), Uloc(3)
          !write(*, *) trierr
          !print*,
  
          call fem2Derr(vrt(1, tri(1, i)), vrt(1, tri(2, i)), vrt(1, tri(3, i)), Lp, &
          & IDEN, FEM_P1, Uloc, Fu1, dData, iData, &
          & label, ANI_D1x1_one, dData, idata, iSys, 2, trierr)
  
  
          if (Lp .eq. 0d0) then
            sol_error = max(sol_error, trierr)
          else 
            sol_error = sol_error + trierr
          end if
        end do
  
        if (Lp .gt. 0) then
          sol_error = sol_error ** (1d0 / Lp)
        end if
    end function
    